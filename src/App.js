import React, { Component } from 'react';
import Header from './layout/Header'
import './App.css';
import 'bootstrap/dist/css/bootstrap.css'

import Login from './components/Login';
import Dashboard from './components/Dashboard';
import {BrowserRouter as Router, Route, Redirect } from 'react-router-dom'
import Logout from './components/Logout';

class App extends Component {

  state = {
    loggedIn: false
  }

  loggedInCheck = (token) => {    
    if (token) {
      this.setState({
        loggedIn: true
      });
    }
  }

  render() {
    return (
      <Router>
      <div className="App">
        <Header loggedIn={this.state.loggedIn}/>
        <Route exact path="/" render={(props) => (
          this.state.loggedIn ? (
            <Redirect to="/home"/>
          ) : (
            <React.Fragment>
                <Login loggedInCheck={this.loggedInCheck}/>
            </React.Fragment>
          )
        )}/> 
        <Route path="/home" render={(props) => (
          this.state.loggedIn ? ( 
            <Route path="/home" component={Dashboard}/>
          ) : (
            <React.Fragment> 
                <Redirect to="/" />
            </React.Fragment>
          )
        )}/>  
        <Route path="/logout" component={Logout}/>
        
      </div>
      </Router>
    );
  }
}

export default App;



