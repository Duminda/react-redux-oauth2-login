import React from 'react';
import { Form, Button, Container, Row, Col } from 'react-bootstrap';
import {connect} from 'react-redux'

import {loginRequest} from '../network/requests'
import {getAuthData} from '../redux/actions'


class Login extends React.Component{

    constructor(props) {
        super(props);

        this.state = {
            username : '',
            password : '',
            grantType : 'password'
        }
      }

    componentWillReceiveProps (nextProps){
        this.props.loggedInCheck(nextProps.accessToken)
    }

    loggedInCheck = (token) => {       
          return token;       
    }

    onChange = (e) => {
        e.preventDefault();
        this.setState({ [e.target.name]: e.target.value})
    }

    onSubmit = (e) => {
        e.preventDefault(); 
        this.props.loginRequest(this.state.username, this.state.password);
        this.setState({ username: '', password : ''})
    }

    render() {
      return (  
        <div>
            <Container>
            <Row>&nbsp;</Row>
            <Row>&nbsp;</Row>
            <Form onSubmit={this.onSubmit}>
                <Row>         
                    <Col xs={8} md={4}></Col>
                    <Col xs={8} md={4}>
                        <Form.Group controlId="formGroupUsername">
                            <Form.Label>Username</Form.Label>
                            <Form.Control type="text" placeholder="Username" value={this.state.username} onChange={this.onChange} name="username"/>
                        </Form.Group>
                        <Form.Group controlId="formGroupPassword">
                            <Form.Label>Password</Form.Label>
                            <Form.Control type="password" placeholder="Password" value={this.state.password}  onChange={this.onChange} name="password"/>
                        </Form.Group>
                        <Button variant="primary" size="lg" block type="submit">
                            Login
                        </Button>
                    </Col>
                    <Col xs={8} md={4}></Col>           
                </Row>
                </Form>          
            </Container>
        </div> 
      )
    }
}

  const mapDispatchToProps = dispatch => ({
    getAuthData: () => dispatch(getAuthData()),
    loginRequest: (username, password) => dispatch(loginRequest(username, password))
  });

  const mapStateToProps = (state) => {
      return {
        accessToken: state.accessToken,
        expiresIn: state.expiresIn,
        refreshExpiresIn: state.refreshExpiresIn,
        refreshToken: state.refreshToken,
        error: state.error,
        errorDescription: state.errorDescription
      }
  }

export default connect(mapStateToProps, mapDispatchToProps)(Login);