import React, {Component} from 'react';
import {Nav, Navbar} from 'react-bootstrap'
import {Link} from 'react-router-dom'

class Header extends Component{

  state = {
    loggedIn: false
  }

  componentWillReceiveProps (nextProps){
    this.setState({loggedIn: nextProps.loggedIn});
  }

    render() {
      return (
        <div>
          <Navbar bg="dark" expand="lg" style={{color:"white"}}>
            <Navbar.Brand href="#home">OAuth2 Flow</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav" >
                <Nav className="justify-content-end">
                    <Link to="/home">Home </Link>                    
                {
                  this.state.loggedIn ? (
                    <Link to="/logout"> &nbsp;Logout</Link>     
                  ) : (
                    <Link to="/"> &nbsp;Login</Link>
                  )
                }                
                </Nav>
                
            </Navbar.Collapse>
            </Navbar>
        </div>
      )
    }

}

export default Header;