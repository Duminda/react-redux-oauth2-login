import {AUTH_SUCCESS, AUTH_ERROR, GET_AUTH_DATA} from '../actionTypes'

const initialState = {
    accessToken: null,
    expiresIn: 0,
    refreshExpiresIn: 0,
    refreshToken: null,
    error: null,
    errorDescription: null
}

export default function(state = initialState, action){

    state = initialState;

    switch(action.type){
        case AUTH_SUCCESS:{
            const {access_token: accessToken,  expires_in: expiresIn,  refresh_expires_in: refreshExpiresIn, refresh_token: refreshToken} = action.payload.data;
            return {
                accessToken: accessToken,
                expiresIn: expiresIn,
                refreshExpiresIn: refreshExpiresIn,
                refreshToken: refreshToken,
                error: null,
                errorDescription: null
            }
        }
        case AUTH_ERROR:{
            const {error: errorName,  error_description: errorDescription} = action.payload.error;
            return {
                ...state,
                error: errorName,
                errorDescription: errorDescription
            }
        }
        case GET_AUTH_DATA:{
            return {
                ...state
            }
        }
        default:
            return state;
    }

}