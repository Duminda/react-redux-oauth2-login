import {AUTH_SUCCESS, AUTH_ERROR, GET_AUTH_DATA} from './actionTypes'

export const saveToken = data => ({
    type: AUTH_SUCCESS,
    payload:{
         data
    }
});

export const authError = error => ({
    type: AUTH_ERROR,
    payload:{
        error
    }
});

export const getAuthData = data => ({
    type: GET_AUTH_DATA,
    payload:{
        data
    }
});