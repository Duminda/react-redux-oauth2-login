
export const AUTH = {
    KEYCLOAK_BASE_URL: "https://188.166.84.136:8443/auth",
    KEYCLOAK_TOKEN_EP: "/realms/calamitaid/protocol/openid-connect/token",
    KEYCLOAK_LOGOUT_EP: "/realms/calamitaid/protocol/openid-connect/logout",
    KEYCLOAK_INTROSPECT_EP: "/realms/calamitaid/protocol/openid-connect/token/introspect",
    KEYCLOAK_USERINFO_EP: "/realms/calamitaid/protocol/openid-connect/userinfo",
    KEYCLOAK_AUTH_HEADER_VALUE: "Basic cmVzdC1hcGktYXV0aDo0ZDlmMzk4NS0xOTFmLTRhYjMtYTAyYy0wMThiZDk5Yzg2OTQ=",   
};