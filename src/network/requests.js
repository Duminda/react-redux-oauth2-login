import axios from 'axios';
import {saveToken, authError} from '../redux/actions'
import {AUTH} from '../constants'

export function loginRequest(username, password) {
        
        return dispatch => {
            
            axios.defaults.baseURL = AUTH.KEYCLOAK_BASE_URL;
        
            const requestBody = `username=${username}&password=${password}&grant_type=password`;
            
            const headers = {
                    'Authorization': AUTH.KEYCLOAK_AUTH_HEADER_VALUE,
                    'Content-Type': 'application/x-www-form-urlencoded'
            }
                
            axios.post(AUTH.KEYCLOAK_TOKEN_EP, requestBody, {headers : headers})
            
            .then(data => {
                dispatch(saveToken(data.data));
                return data.data;                
            })
            .catch(error => {
                dispatch(authError(error.response.data));
            });    
        } 

}

export function tokenIntrospectRequest(token) {

}

export function tokenRefreshRequest(refreshToken) {

}

export function logoutRequest(refreshToken) {

}